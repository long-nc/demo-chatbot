#!/bin/sh

set -m

cd backend
./run_rasa.sh &
python3 ./app.py prod &
cd ../frontend
# serve -s build -l 3000 &
npm start &

fg %1
