## Demo chatbot

### Build docker

`docker build . -t user/demo-chatbot`

### Run docker

`docker run -it -p 3000:3000 -p 5000:5000 -p 5005:5005 user/demo-chatbot`

### Run docker in background

`docker run -dit -p 3000:3000 -p 5000:5000 -p 5005:5005 user/demo-chatbot`

### TODO

- [ ] Integrate facebook msg on web  
- [ ] Dynamic action on entity  
