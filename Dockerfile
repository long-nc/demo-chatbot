# syntax=docker/dockerfile:1
FROM nikolaik/python-nodejs:python3.7-nodejs12

# setup working directory
# USER user
WORKDIR /home/user/chatbot-demo
COPY . .

# installing packages
RUN pip3 install -r backend/requirements.txt
RUN cd frontend && npm install

# build frontend
# RUN cd frontend && npm run build
# RUN npm i -g serve

# forward port
EXPOSE 5005
EXPOSE 3000
EXPOSE 5000

# run
CMD ./run.sh
