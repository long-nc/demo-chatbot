const origin = window.location.origin
const lastColonIdx = origin.lastIndexOf(':')
const hostname = lastColonIdx <= 5 ? origin : origin.substr(0, origin.lastIndexOf(":"))
const backendUrl = `${hostname}:5000`

export {hostname, backendUrl}