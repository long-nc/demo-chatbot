import React, {useState, Fragment} from "react";
import "../App.css";
import ListEntity from "../component/ListEntity";
import PopUpCreateEntity from "../base/PopUpCreateEntity";
import axios from "axios";
import PopUpCreateIntent from "../base/PopUpCreateIntent";
import dropdown from '../access/img/icon_down.png';
import Loader from 'react-loader-spinner';
import {backendUrl} from '../constants';

function Card({card, onChangeCard}) {
  const {title, subtitle, image, buttons} = card
  return (
    <div style={{borderRadius: '8px', backgroundColor: '#ccc', padding: '16px', border: '1px solid #ccc', display: 'flex', flexDirection: 'column', marginRight: '24px'}}>
      <input value={title} placeholder='Nhập tiêu đề' style={{marginBottom: '8px'}} onChange={e => onChangeCard({...card, title: e.target.value})} />
      <input value={subtitle} placeholder='Nhập mô tả' style={{marginBottom: '8px'}} onChange={e => onChangeCard({...card, subtitle: e.target.value})} />
      <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', marginBottom: '8px'}}>
        <img src={image || 'https://via.placeholder.com/200/200'} alt={'ảnh thẻ'}
        onClick={() => {
          const url = prompt("Nhập url ảnh", image)
          if (url) {
            onChangeCard({...card, image: url})
          }
        }}
        style={{
          width: '150px', height: '150px',
          objectFit: 'cover'
        }} />
      </div>
      {buttons && buttons.map((b, idx) => (
        <button key={idx} onClick={() => {
          const text = prompt("Nhập tên nút", b.text) 
          if (text) {
            const btns = Array.from(card.buttons)
            btns[idx] = {text}
            onChangeCard({
              ...card,
              buttons: btns
            })
          }
        }}>{b.text}</button>
      ))}
      <button onClick={() => onChangeCard({...card, buttons: [...card.buttons, {text: '<rỗng>'}]})}>+</button>
    </div>
  );
}

function Home({location}) {
  const [showPopup, setShowpopup] = useState(false);
  const [popupIntent, setPopupIntent] = useState(false);
  const [checkSame, setCheckSame] = useState(false);
  const [textInput, setTextInput] = useState("");
  const [utter, setUtter] = useState("");
  const [title, setTitle] = useState("");
  const [entitySelect, setEntitySelect] = useState("");
  const [editing, setEditing] = useState(false);
  const [listDataEntity, setListDataEntity] = useState([]);
  const [textInputEnity, setTextInputEnity] = useState("");
  const [example, setExample] = useState("");
  const [training, setTraining] = useState(false);
  const [utterMode, setUtterMode] = useState('basic'); // basic, advanced
  const [additionalCards, setAdditionalCards] = useState([]);
  const train = async () => {
    try {
      setTraining(true)
      await axios.get(backendUrl + '/train')
      setTraining(false)
    } catch (e) {
      console.log(e)
    }
  }

  const [selectedIntent, setSelectedIntent] = useState('');
  const onSelectEntity = entity => {
    setExample(prev => {
      if (prev) return prev.replace(entitySelect, `[${entitySelect}]{"entity": "${entity}"}`)
      else return textInput.replace(entitySelect, `[${entitySelect}]{"entity": "${entity}"}`)
    })
    setListDataEntity(prev => ([
      ...prev,
      { name: entitySelect, entity: entity},
    ]));
  }
  const onRemoveEntity = idx => {
    console.log(idx)
    const { name, entity }= listDataEntity[idx]
    setExample(prev => {
      return prev.replace(`[${name}]{"entity": "${entity}"}`, name)
    })
    setListDataEntity(prev => {
      return prev.filter((it, currIdx) => currIdx !== idx)
    })
  }

  const handleMouseUp = () => {
    var res = window.getSelection().toString();
    if (res) {
      setShowpopup(true)
    } else {
      setShowpopup(false)
    }
    setEntitySelect(res)
  };

  const onClickPopUpCreate = () => {
    setShowpopup(false);
  };
  const openPopup = () => {
    !checkSame && setPopupIntent(true);
    setCheckSame(false);
  };

  const onClickOutside = () => {
    setPopupIntent(false);
    setCheckSame(true);
    setTimeout(() => {
      setCheckSame(false);
    }, 100);
  };

  const onClickBtn = () => {
    setListDataEntity([
      ...listDataEntity,
      { entity: textInputEnity, valueSelect: entitySelect },
    ]);
    setShowpopup(false);
  };
  const handlerEditing = () => {
    setEditing(!editing);
  };
  const onChangeText = (text) => {
    console.log(text);
    setTextInputEnity(text);
  };

  const saveStory = async () => {
    console.log(example);
    try {
      await axios.post(backendUrl + '/intent', {
        name: selectedIntent, example: example ? example : textInput
      })
      let utterObj;
      if (utterMode === 'advanced') {
        utterObj = {name: `utter_${new Date().getTime()}`, text: utter}
        utterObj.elements = additionalCards.map(c => ({
          title: c.title,
          subtitle: c.subtitle,
          image_url: c.image,
          buttons: c.buttons.map(b => ({
            title: b.text,
          }))
        }))
      } else {
        utterObj = {name: `utter_${new Date().getTime()}`, text: utter}
      }
      await axios.post(backendUrl + '/story', {name: title, intent: selectedIntent, utter: utterObj})
      setTitle('')
      setTextInput('')
      setUtter('')
      setListDataEntity([])
      setSelectedIntent('')
      setAdditionalCards([])
      setUtterMode('basic')
      alert('Thành công')
    } catch (e) {
      alert('Thất bại')
    }
  }

  return (
    <div className="table_conten">
      {training && <div
        style={{
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          display: "flex",
          justifyContent: 'center',
          alignItems: 'center',
          position: "absolute",
          backgroundColor: 'rgba(0,0,0, 0.2)',
          zIndex: 9999
        }}
      >
          <Loader type="ThreeDots" color="#5c3bbf" height="100" width="100" />
      </div>}
      <div>
        <h2>Huấn luyện bot</h2>
        <div className="textDescribe">
          Nhập mẫu câu ý định và câu trả lời. Bạn cũng có thể đánh dấu các thực thể trong câu.
        </div>
        <div style={{display: 'flex'}}>
          <div style={{flex: 1}} />
          <button onClick={train} className="btnTrain" disabled={training} style={{backgroundColor: training ? 'rgba(92, 59, 191, 0.2)' : '#5c3bbf'}}>
              Tiến hành học
          </button>
        </div>
        {/* input */}
        <div className="inputBox">
          <input
            placeholder="Tiêu đề"
            className="inputTag"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>
        <div className="inputBox">
          {editing ? (
            <Fragment>
              <div
                onMouseUp={handleMouseUp}
                id="editor"
                style={{ fontSize: 18, marginLeft: 10 }}
                className="input"
              >
                {!!textInput ? Array.from(textInput).map((char, index) => (
                  <span key={index}>{char}</span>
                )) : "Ý định"}
              </div>
              <h3 className="btnSave" onClick={handlerEditing}>
                Sửa
              </h3>
            </Fragment>
          ) : (
            <Fragment>
              <input
                placeholder="Ý định"
                className="inputTag"
                value={textInput}
                onChange={(e) => setTextInput(e.target.value)}
              />
              <button className="btnSave" onClick={handlerEditing}>
                Nhập xong
              </button>
            </Fragment>
          )}
        </div>
        <div className="inputArea">
          <textarea
            placeholder="Phản hồi"
            className="textareaTag"
            value={utter}
            rows={1}
            onChange={(e) => setUtter(e.target.value)}
          />
          <button className="btnToggleUtter" onClick={() => {
            if (utterMode === 'advanced') {
              setAdditionalCards([])
            }
            setUtterMode(prev => prev === 'basic' ? 'advanced' : 'basic')
          }}>
            {utterMode === 'basic' ? "Cơ bản" : "Nâng cao"}  
          </button>
        </div>
        {utterMode === 'advanced' && <div className="advanced-utter">
          {additionalCards.map((c, idx) => {
            return <Card key={idx} card={c} onChangeCard={(card) => {
              setAdditionalCards(prev => {
                const next = Array.from(prev)
                next[idx] = card
                return next
              })
            }} />  
          })}
          <button
            onClick={() => {
              setAdditionalCards(prev => [...prev, {
                title: '', subtitle: '', image: '', buttons: []
              }])
            }}
            style={{width: '64px', height: '64px', borderRadius: '32px', backgroundColor: '#2196f3', fontSize: '24pt', color: 'white', border: '1px solid #2196f3'}}>
            +
          </button>
        </div>}
        {showPopup && (
          <PopUpCreateEntity
            width={"60%"}
            show={showPopup}
            placeholderInput={textInputEnity}
            btnText={"Tạo thực thể"}
            onClickOutside={onClickPopUpCreate}
            onChangeText={onChangeText}
            onSelect={onSelectEntity}
          />
        )}
        <div className="choose_intent">
          <div className="conten_1">
            <div>Ý định</div>
            <div onClick={openPopup} className="conten_2">
              <div style={{ fontSize: 14, marginLeft: 10 }}>
                {selectedIntent ? selectedIntent : 'Chọn hoặc thêm ý định'}
              </div>
              <img
                alt={"dropdown"}
                style={{ width: 10, height: 10, marginRight: 15 }}
                src={dropdown}
              />
            </div>
            {popupIntent && (
              <PopUpCreateIntent
                width={"60%"}
                top={"42px"}
                left={"53px"}
                btnText={"Tạo ý định"}
                show={popupIntent}
                onClickOutside={onClickOutside}
                onSelect={setSelectedIntent}
                // placeholderInput={entitySelect}
                onClickBtn={onClickBtn}
                // onChangeText={onChangeText}
              />
            )}
          </div>
        </div>
        <ListEntity data={listDataEntity} setData={onRemoveEntity} />
        <button onClick={saveStory} className="noti" style={{marginTop: '36px', marginBottom: '36px', fontSize: '18px', width: '200px'}}>
          Thêm kịch bản
        </button>
      </div>
    </div>
  );
}

export default Home;
