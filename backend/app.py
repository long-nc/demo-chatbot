from flask import Flask, redirect, url_for, request
from flask_cors import CORS, cross_origin
from yaml_utils import manipulate_yaml, check_for_existed_child
from pathlib import Path
import json
import time
import requests
import os
import rasa

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['JSON_AS_ASCII'] = False

@app.route('/health_check')
def health_check():
    return "OK"

# intent
@app.route('/intent', methods=['GET', 'POST'])
def list_intents():
    nlu_path = Path('rasa_app/data/nlu.yml').resolve()
    domain_path = Path('rasa_app/domain.yml').resolve()
    if request.method == 'POST':
        body = request.get_json()
        print(f'adding intent: {body}')
        name = body['name']
        example = body['example']
        # add in data/nlu.yml, domain.yml
        existed, key = check_for_existed_child(domain_path, 'intents', {name: {'use_entities': True}})
        print('exist' +  str(existed))
        if not existed:
            manipulate_yaml(domain_path, f'intents.-1', 'w', {name: {'use_entities': True}})

        existed, key = check_for_existed_child(nlu_path, 'nlu', {'intent': name})
        print(key)
        if existed:
            examples = manipulate_yaml(nlu_path, f'nlu.{key}.examples', 'r')
            examples = examples + (f'- {example}\n' if len(example) > 0 else '')
            return json.dumps(manipulate_yaml(nlu_path, f'nlu.{key}.examples', 'w', examples)['nlu'])
        else:
            return json.dumps(manipulate_yaml(nlu_path, 'nlu.-1', 'w', {'intent': name, 'examples': f'- {example}\n' if len(example) > 0 else ''})['nlu'])

    elif request.method == 'GET':
        intents = manipulate_yaml(nlu_path, 'nlu', 'r')
        return json.dumps(intents)

@app.route('/intent/<idx>', methods=['DELETE'])
def delete_intent(idx):
    nlu_path = Path('rasa_app/data/nlu.yml').resolve()
    domain_path = Path('rasa_app/domain.yml').resolve()
    manipulate_yaml(domain_path, f'intents.{idx}', 'w', isDelete = True)
    intents = manipulate_yaml(nlu_path, f'nlu.{idx}', 'w', isDelete = True)
    print(intents)
    return json.dumps(intents['nlu'])

@app.route('/intent/<idx>', methods=['PUT'])
def update_intent(idx):
    examples = request.get_json()['examples']
    path = Path('rasa_app/data/nlu.yml').resolve()
    intents = manipulate_yaml(path, f'nlu.{idx}.examples', 'w', examples)
    return json.dumps(intents['nlu'])


# entity
@app.route('/entity', methods=['GET', 'POST'])
def list_entities():
    domain_path = Path('rasa_app/domain.yml').resolve()
    if request.method == 'POST':
        body = request.get_json()
        print(f'adding entity: {body}')
        name = body['name']
        # add in domain.yml
        existed, key = check_for_existed_child(domain_path, 'entities', name)
        if not existed:
            return json.dumps(manipulate_yaml(domain_path, f'entities.-1', 'w', name)['entities'])
        else:
            return json.dumps(manipulate_yaml(domain_path, '', 'r')['entities'])

    elif request.method == 'GET':
        entities = manipulate_yaml(domain_path, 'entities', 'r')
        return json.dumps(entities)

@app.route('/entity/<int:idx>', methods=['DELETE'])
def delete_entity(idx):
    path = Path('rasa_app/domain.yml').resolve()
    entities = manipulate_yaml(path, f'entities.{idx}', 'w', isDelete = True)
    return json.dumps(entities['entities'])

# story
@app.route('/story', methods=['GET', 'POST'])
def list_stories():
    stories_path = Path('rasa_app/data/stories.yml').resolve()
    domain_path = Path('rasa_app/domain.yml').resolve()
    if request.method == 'POST':
        body = request.get_json()
        print(f'adding story: {body}')
        name = body['name']
        intent = body['intent']
        utter = body['utter']
        utter_name = utter['name']
        del utter['name']
        # add in domain.yml, data/stories.yml
        manipulate_yaml(domain_path, f'responses.{utter_name}', 'w', [utter])
        return json.dumps(manipulate_yaml(stories_path, f'stories.-1', 'w', {'story': name, 'steps': [{'intent': intent}, {'action': utter_name}]})['stories'])

    elif request.method == 'GET':
        stories = manipulate_yaml(stories_path, 'stories', 'r')
        for s in stories:
            s['steps'][1]['action'] = manipulate_yaml(domain_path, f'responses.{s["steps"][1]["action"]}', 'r')[0]['text']
        return json.dumps(stories)

@app.route('/story/<int:idx>', methods=['DELETE'])
def delete_story(idx):
    path = Path('rasa_app/data/stories.yml').resolve()
    stories = manipulate_yaml(path, f'stories.{idx}', 'w', isDelete = True)
    return json.dumps(stories['stories'])

@app.route('/train')
def train():
    import subprocess
    process = subprocess.Popen('./train_rasa.sh', shell=True, stdout=subprocess.PIPE)
    process.wait()
    model_path = Path('./models').resolve()
    print(os.path.join(model_path, 'model.tar.gz'))
    r = requests.put('http://localhost:5005/model?token=thisismysecret', headers={'Accept': 'application/json'}, json={'model_file': os.path.join(model_path, 'model.tar.gz')})
    print(r.text)
    if r.status_code == 204:
        return json.dumps({'message': 'OK'})
    else:
        return json.dumps({'message': 'Failed'})

@app.route('/chat', methods=['POST'])
def chat():
    message = request.get_json()['message']
    sender = request.get_json()['sender']
    r = requests.post('http://localhost:5005/webhooks/rest/webhook', headers={'Accept': 'application/json'}, json={'message': message, 'sender': sender})
    print(r.text)
    if r.status_code == 200:
        return json.dumps(r.json())
    else:
        return json.dumps([])

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1 and sys.argv[1] == 'prod':
        app.run(host='0.0.0.0' , port=5000, debug = False)
    else:
        app.run(host='0.0.0.0' , port=5000, debug = True)
    # app.run(debug = True)
