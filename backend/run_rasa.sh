#!/bin/sh

rasa run \
    -m models \
    --enable-api \
    --log-file out.log \
    --auth-token thisismysecret \
    --credentials rasa_app/credentials.yml \
    --endpoints rasa_app/endpoints.yml
