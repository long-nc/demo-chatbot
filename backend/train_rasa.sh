#!/bin/sh

rasa train --domain rasa_app/domain.yml --config rasa_app/config.yml --data rasa_app/data --fixed-model-name model
