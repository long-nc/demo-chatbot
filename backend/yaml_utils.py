from yaml import safe_load, dump
import os

def manipulate_yaml(filepath: str, target: str, option: str = 'r', value = '', isDelete: bool = False):
    data = _open_yaml(filepath)
    try:
        targetPaths = target.split('.')
        elementData = data

        # access to the n - 1 path to maintain reference
        for p in targetPaths[:-1]:
            if len(p) == 0:
                return elementData
            if type(elementData) in (tuple, list):
                parsed_p = int(p)
                if len(elementData) > parsed_p:
                    elementData = elementData[parsed_p]
                elif option == 'r':
                    return None
                else:
                    elementData[parsed_p] = dict()
                    elementData = elementData[parsed_p]
            else:
                if p in elementData:
                    elementData = elementData[p]
                elif option == 'r':
                    return None
                else:
                    elementData[p] = dict()
                    elementData = elementData[p]

        lastPath = targetPaths[-1]
        if option == 'r':
            if len(lastPath) == 0:
                return elementData
            elif type(elementData) in (tuple, list):
                return elementData[int(lastPath)]
            else:
                return elementData[lastPath]
        elif isDelete:
            if len(lastPath) == 0:
                data = dict()
            elif type(elementData) in (tuple, list):
                del elementData[int(lastPath)]
            else:
                del elementData[lastPath]
            _save_yaml(filepath, data)
            return data
        else:
            if len(lastPath) == 0:
                data = value
            elif type(elementData) in (tuple, list):
                parsedLastPath = int(lastPath)
                if parsedLastPath == -1:
                    elementData.append(value)
                else:
                    for i in range(len(elementData), parsedLastPath + 1):
                        if i == parsedLastPath:
                            elementData.append(value)
                        else:
                            elementData.append('')
            else:
                elementData[lastPath] = value
            _save_yaml(filepath, data)
            return data
    except Exception as e:
        print('Failed to parse target path of ' + target)
        print(e)
        return None

def _open_yaml(filepath: str):

    if not os.path.exists(filepath):
        print('File not found ' + filepath)
        return None

    with open(filepath, 'r') as file:
        try:
            data = safe_load(file.read())
            return data
        except Exception as e:
            print('Failed to read yaml file at ' + filepath)
            print(e)
            return None

def _save_yaml(filepath: str, data: dict):
    with open(filepath, 'w') as file:
        try:
            dump(data, file, encoding='utf-8', allow_unicode=True)
        except Exception as e:
            print('Failed to save yaml file at ' + filepath)
            print(e)
            return None

def check_for_existed_child(filepath: str, parentPath: str, searchingVal):
    def compare(src, dst):
        if type(src) != type(dst):
            return False
        if type(src) in (tuple, list):
            return src == dst
        if type(src) == dict:
            for key, value in dst.items():
                if key not in src or src[key] != value:
                    return False
            return True
        return src == dst

    parent = manipulate_yaml(filepath, parentPath, 'r')
    if type(parent) in (tuple, list):
        for idx, child in enumerate(parent):
            if compare(child, searchingVal): return True, idx
        return False, None
    elif type(parent) == dict:
        for key, value in parent.items():
            if compare(value, searchingVal): return True, key
        return False, None
    else:
        return False, None


if __name__ == '__main__':
    print('=======* test dictionary *=======')
    open('domain.yml', 'a').close()
    # test add
    print('test add')
    manipulate_yaml('domain.yml', '', 'w', {'version': '2.0'})
    print(manipulate_yaml('domain.yml', '', 'r'))
    manipulate_yaml('domain.yml', 'versionb',  'w', '2.0b')
    print(manipulate_yaml('domain.yml', '', 'r'))
    print('============')

    # test delete
    print('test delete')
    manipulate_yaml('domain.yml', '', 'w', {'version': '2.0'})
    print(manipulate_yaml('domain.yml', '', 'r'))
    manipulate_yaml('domain.yml', 'version', 'w', isDelete = True)
    print(manipulate_yaml('domain.yml', '', 'r'))
    print('============')

    # test edit
    print('test edit')
    manipulate_yaml('domain.yml', '', 'w', {'version': '2.0'})
    print(manipulate_yaml('domain.yml', '', 'r'))
    manipulate_yaml('domain.yml', 'version', 'w', '1.0')
    print(manipulate_yaml('domain.yml', '', 'r'))
    print('============')
    os.remove('domain.yml')

    print('=======* test array *=======')
    open('domain.yml', 'a').close()
    # test add
    print('test add')
    manipulate_yaml('domain.yml', '', 'w', [1,2,3])
    print(manipulate_yaml('domain.yml', '', 'r'))
    manipulate_yaml('domain.yml', '6',  'w', 4)
    print(manipulate_yaml('domain.yml', '', 'r'))
    print('============')

    # test delete
    print('test delete')
    manipulate_yaml('domain.yml', '', 'w', [1,2,3])
    print(manipulate_yaml('domain.yml', '', 'r'))
    manipulate_yaml('domain.yml', '2', 'w', isDelete = True)
    print(manipulate_yaml('domain.yml', '', 'r'))
    print('============')

    # test edit
    print('test edit')
    manipulate_yaml('domain.yml', '', 'w', [1,2,3])
    print(manipulate_yaml('domain.yml', '', 'r'))
    manipulate_yaml('domain.yml', '2', 'w', 4)
    print(manipulate_yaml('domain.yml', '', 'r'))
    print('============')
    os.remove('domain.yml')

    print('=======* test check exist dictionary *=======')
    open('domain.yml', 'a').close()
    # exist
    print('test exist')
    manipulate_yaml('domain.yml', '', 'w', {'a': 1, 'b': 2})
    print(manipulate_yaml('domain.yml', '', 'r'))
    compareFactor = 1
    print(f'1 exists: {check_for_existed_child("domain.yml", "", compareFactor)}')

    # not exist
    print('test not exist')
    manipulate_yaml('domain.yml', '', 'w', {'a': 1, 'b': 2})
    print(manipulate_yaml('domain.yml', '', 'r'))
    compareFactor = 3
    print(f'3 exists: {check_for_existed_child("domain.yml", "", compareFactor)}')
    os.remove('domain.yml')

    print('=======* test check exist array *=======')
    open('domain.yml', 'a').close()
    # exist
    print('test exist')
    manipulate_yaml('domain.yml', '', 'w', [1,2,3])
    print(manipulate_yaml('domain.yml', '', 'r'))
    print(f'3 exists: {check_for_existed_child("domain.yml", "", 3)}')

    # not exist
    print('test not exist')
    manipulate_yaml('domain.yml', '', 'w', [1,2,3])
    print(manipulate_yaml('domain.yml', '', 'r'))
    print(f'4 exists: {check_for_existed_child("domain.yml", "", 4)}')
    os.remove('domain.yml')
